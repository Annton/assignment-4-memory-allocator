//
// Created by anton on 16.12.2023.
//

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <sys/mman.h>

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
void debug(const char* fmt, ... );


//Обычное успешное выделение памяти.
void test1(){
    debug("Test 1: tying to allocate the memory\n");
    void* test_heap= heap_init(REGION_MIN_SIZE);
    assert(test_heap);
    void* test_alloc= _malloc(52);
    assert(test_alloc);
    _free(test_alloc);
    heap_term();
    debug("Test 1 passed\n");

}
//Освобождение одного блока из нескольких выделенных.
void test2(){
    debug("Test 2: trying to free allocated block\n");
    void* test_heap= heap_init(REGION_MIN_SIZE);
    assert(test_heap);
    void* first_block= _malloc(735);
    void* second_block= _malloc(812);
    assert(first_block);
    assert(second_block);
    _free(first_block);
    assert(block_get_header(first_block)->is_free);
    assert(!block_get_header(second_block)->is_free);
    heap_term();
    debug("Test 2 passed\n");



}
//Освобождение двух блоков из нескольких выделенных.
void test3(){
    debug("Test 3: trying to free two allocated block\n");
    void* test_heap= heap_init(REGION_MIN_SIZE);
    assert(test_heap);
    void* first_block= _malloc(735);
    void* second_block= _malloc(812);
    assert(first_block);
    assert(second_block);
    _free(first_block);
    _free(second_block);
    assert(block_get_header(first_block)->is_free);
    assert(block_get_header(second_block)->is_free);
    heap_term();
    debug("Test 3 passed\n");

}

//Память закончилась, новый регион памяти расширяет старый.
void test4(){
    debug("Test 4: memory is over, trying to expand memory region\n ");
    void* test_heap= heap_init(REGION_MIN_SIZE);
    assert(test_heap);
    void* data= _malloc(REGION_MIN_SIZE);
    assert(data);
    assert(block_get_header(data)->capacity.bytes==REGION_MIN_SIZE);
    heap_term();
    debug("Test 4 passed\n");

}
//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void test5(){
    debug("Test 5: trying to allocate new region somewhere else");
    void* pre_alloc= map_pages(HEAP_START, 52, MAP_FIXED);
    assert(pre_alloc);
    void* alloc= _malloc(52);
    assert(alloc);
    assert(alloc!=pre_alloc);
    heap_term();
    debug("Test 5 passed");


}


int main(){
    debug("---------------------TESTING----------------------");
    test1();
    test2();
    test3();
    test4();
    test5();
    debug("All tests passed");
    return 0;

}
